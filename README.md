# FVSCL

Fluid Visual Scaling Component Library

[Visit the demo site](https://superallan.gitlab.io/fvscl/)

## Getting started

Get the code and start playing.

```bash
git clone git@gitlab.com:superallan/fvscl.git
npm install # or yarn
npm run dev
```

Open up [localhost:3000](http://localhost:3000) and start clicking around.
